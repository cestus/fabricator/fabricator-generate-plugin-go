
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.1.0"></a>
## v0.1.0

### Feat

* add version.go generation
* use code.cestus.io/libs/buildinfo
* add plugin generation code

